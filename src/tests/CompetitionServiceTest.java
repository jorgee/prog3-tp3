package tests;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import entities.Competition;
import entities.Group;
import entities.Team;
import main.App;
import services.CompetitionService;
import services.CombinatoricsGeneratorService;

public class CompetitionServiceTest {

	@Mock
    App appMock; 

    @Rule public MockitoRule mockitoRule = MockitoJUnit.rule(); 

	@Test
	/**
	 * Test combinatoric (4,2)
	 */
	public void testGroupGenerationOf2MembersOutOf4() {
		CombinatoricsGeneratorService<Team> groupGenerator = new CombinatoricsGeneratorService<Team>(this.appMock);
		CombinatoricsGeneratorService<Group> competitionGenerator = new CombinatoricsGeneratorService<Group>(this.appMock);
		CompetitionService competitionService = new CompetitionService(appMock, "Copa América", groupGenerator, competitionGenerator);
		Team team1 = new Team("Argentina", (float) 1.9);
		Team team2 = new Team("Uruguay", (float) 2.9);
		Team team3 = new Team("Paraguay", (float) 1.5);
		Team team4 = new Team("Brasil", (float) 1.91);
		Team team5 = new Team("Perú", (float) 1.88);
		Team team6 = new Team("Bolivia", (float) 2.4);
		Team team7 = new Team("Venezuela", (float) 1.65);
		Team team8 = new Team("Colombia", (float) 1.32);
		Team team9 = new Team("Chile", (float) 1.48);
		Team team10 = new Team("Ecuador", (float) 2.33);
		Team team11 = new Team("Japón", (float) 1.21);
		Team team12 = new Team("Catar", (float) 1.54);
		competitionService.registerTeam(team1);
		competitionService.registerTeam(team2);
		competitionService.registerTeam(team3);
		competitionService.registerTeam(team4);
		competitionService.registerTeam(team5);
		competitionService.registerTeam(team6);
		competitionService.registerTeam(team7);
		competitionService.registerTeam(team8);
		competitionService.registerTeam(team9);
		competitionService.registerTeam(team10);
		competitionService.registerTeam(team11);
		competitionService.registerTeam(team12);

		Competition c = competitionService.getBalancedGroupsCompetition();
		HashSet<Team> teams = new HashSet<Team>();
		// Agregar todos los equipos de la competicion C a teams que es un HashSet. Por lo tanto todos los equipos deben
		// ser diferentes.
		int i = 1;
		for (Group group : c.getGroups()) {
			teams.addAll(group.getTeams());
			System.out.print(String.format("Grupo %d (disp %f): ", 
					i, group.getDispersion())
			);
			for (Team team : group.getTeams()) {
				System.out.print(String.format("%s (cf %f)  ", team.getName(), team.getCoeficienteDeFuerza()));				
			}
			System.out.println();
			i++;
		}
		
		assertEquals(3, c.getGroups().size());
		assertEquals(12, teams.size());
	}
}
