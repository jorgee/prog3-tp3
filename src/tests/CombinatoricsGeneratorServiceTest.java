package tests;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashSet;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import main.App;
import services.CombinatoricsGeneratorService;

public class CombinatoricsGeneratorServiceTest {
	
	@Mock
    App AppMock; 

    @Rule public MockitoRule mockitoRule = MockitoJUnit.rule(); 

	@Test
	/**
	 * Test combinatoric (4,2)
	 */
	public void testGroupGenerationOf2MembersOutOf4() {
		CombinatoricsGeneratorService<Integer> groupGenerator = new CombinatoricsGeneratorService<Integer>(this.AppMock);
		HashSet<Integer> membersList = new HashSet<Integer>();
		membersList.add(1);
		membersList.add(2);
		membersList.add(3);
		membersList.add(4);
		
		ArrayList<HashSet<Integer>> result = groupGenerator.generateCombinatoric(membersList, 2);
		assertEquals(6, result.size());
	}
	
	@Test
	/**
	 * Test combinatoric (4,4)
	 */
	public void testGroupGenerationOf4MembersOutOf4() {
		CombinatoricsGeneratorService<Integer> groupGenerator = new CombinatoricsGeneratorService<Integer>(this.AppMock);
		HashSet<Integer> membersList = new HashSet<Integer>();
		membersList.add(1);
		membersList.add(2);
		membersList.add(3);
		membersList.add(4);
		
		ArrayList<HashSet<Integer>> result = groupGenerator.generateCombinatoric(membersList, 4);
		assertEquals(1, result.size());
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testNullArgumentThrowsException() {
		CombinatoricsGeneratorService<Integer> groupGenerator = new CombinatoricsGeneratorService<Integer>(this.AppMock);
		groupGenerator.generateCombinatoric(null, 1);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testMembersByGroupThrowsExceptionIfBiggerThanMembersSize() {
		CombinatoricsGeneratorService<Integer> groupGenerator = new CombinatoricsGeneratorService<Integer>(this.AppMock);
		HashSet<Integer> membersList = new HashSet<Integer>();
		membersList.add(1);
		groupGenerator.generateCombinatoric(membersList, 2);
	}
}
