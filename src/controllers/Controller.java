package controllers;

import main.App;
import services.Service;

public abstract class Controller extends Service {
		
	public Controller(App app) {
		super(app);
		this.app.getDisplay().setController(this);
	}
	
	public void render(String viewName) throws Exception {
		this.app.getDisplay().render(viewName, false);
	}
	
	public void render(String viewName, boolean recreate) throws Exception {
		this.app.getDisplay().render(viewName, recreate);
	}
}
