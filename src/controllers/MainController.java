package controllers;

import entities.Competition;
import entities.Team;
import main.App;
import services.CompetitionService;

public class MainController extends Controller {
	
	CompetitionService competitionService;
	
	public MainController(App app, CompetitionService competitionService) {
		super(app);
		this.competitionService = competitionService;
	}

	public void actionMain() {
		try {
			this.render("Main");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void actionRegisterTeam(Team team) {
		this.competitionService.registerTeam(team);
	}
	
	public Competition getBalancedGroupsCompetition() {
		return this.competitionService.getBalancedGroupsCompetition();
	}
}
