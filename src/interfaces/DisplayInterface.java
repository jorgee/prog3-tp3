package interfaces;

import controllers.Controller;

public interface DisplayInterface {
	
	public Controller getController();
	public void setController(Controller appController);
	public void render(String viewName) throws Exception;
	public void render(String viewName, boolean recreate) throws Exception;
}
