package entities;

import java.util.HashSet;

public class Group {

	protected HashSet<Team> teams;
	
	private float dispersion;
	
	public Group(HashSet<Team> teamsSubset) {
		this.teams = teamsSubset;
	}
	
	/**
	 * Debe calcularse siempre porque podría modificarse algún miembro de teams con una
	 * referencia al objeto.
	 * @return
	 */
	public float getDispersion() {
		float dispersion = 0;
		float promedio = 0;
		for (Team team: this.teams) {
			promedio += team.getCoeficienteDeFuerza();
		}
		promedio = promedio / this.teams.size();
		for (Team team: this.teams) {
			dispersion += Math.pow(team.getCoeficienteDeFuerza() - promedio, 2);
		}
		this.dispersion = dispersion / (this.teams.size() - 1);
		return this.dispersion;
	}
	
	public HashSet<Team> getTeams() {
		return this.teams;
	}
}
