package entities;

public class Team {
	
	protected float coeficienteDeFuerza;
	protected String name;
	
	public Team(String name, float coeficienteDeFuerza) {
		if (coeficienteDeFuerza <= 0) {
			throw new IllegalArgumentException("El coeficiente de fuerza debe ser mayor a 0");
		}
		this.name = name;
		this.coeficienteDeFuerza = coeficienteDeFuerza;
	}
	
	public float getCoeficienteDeFuerza() {
		return this.coeficienteDeFuerza;
	}
	
	public void setCoeficienteDeFuerza(float cf) {
		this.coeficienteDeFuerza = cf;
	}

	public String getName() {
		return name;
	}
}
