package entities;

import java.util.HashSet;

public class Competition {
	
	protected HashSet<Group> groups;
	protected String name;
	
	public Competition(String name, HashSet<Group> groups) {
		this.groups = groups;
		this.name = name;
	}
	
	public HashSet<Group> getGroups() {
		return this.groups;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getTotalDispersion() {
		float dispersionSum = 0;
		for (Group group : this.groups) {
			dispersionSum += group.getDispersion();
		}
		return dispersionSum;
	}
}
