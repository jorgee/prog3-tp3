package services;

import main.App;
import main.AppState;

public abstract class Service {
	
	protected App app;
	
	public Service(App app) {
		this.app = app;
	}
	
	public AppState getAppState() {
		return this.app.getAppState();
	}
}
