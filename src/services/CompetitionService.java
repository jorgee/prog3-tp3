package services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

import entities.Competition;
import entities.Group;
import entities.Team;
import main.App;

public class CompetitionService extends Service {
	
	HashSet<Team> teams;

	CombinatoricsGeneratorService<Team> groupGeneratorService;
	CombinatoricsGeneratorService<Group> competitionGeneratorService;
	int groupsPerCompetition = 3;
	int teamsPerGroup = 4;
	String competitionName;
	
	public CompetitionService(
			App app,
			String competitionName,
			CombinatoricsGeneratorService<Team> groupGeneratorService, 
			CombinatoricsGeneratorService<Group> competitionGeneratorService
	) {
		super(app);
		this.competitionName = competitionName;
		this.groupGeneratorService = groupGeneratorService;
		this.competitionGeneratorService = competitionGeneratorService;
		this.teams = new HashSet<Team>();
	}

	public void registerTeam(Team team) {
		this.teams.add(team);
	}
	
	public Competition getBalancedGroupsCompetition() {
		
		// Genera sets de 4 equipos (Team).
		ArrayList<HashSet<Team>> teamsCombinatoric = this.groupGeneratorService.generateCombinatoric(this.teams, this.teamsPerGroup);
		
		// Transforma cada set de equipos a un grupo (Grupo).
		ArrayList<Group> groups = this.transformTeamsCombinatoricIntoGroup(teamsCombinatoric);
		
		// Ordenar de mayor a menor dispersión
		Collections.sort(groups, new Comparator<Group>() {
		    @Override
		    public int compare(Group o1, Group o2) {
		        if (o1.getDispersion() > o2.getDispersion()) {
		        	return -1;
		        }
		        if (o1.getDispersion() < o2.getDispersion()) {
		        	return 1;
		        }
		        return 0;
		    }
		});
		System.out.println("Grupos posibles: " + groups.size());


		// Armar competencias de 3 grupos con los primeros N grupos y ver si tienen todos equipos distintos.
		int competitionCounter = 1;
		for (int i = 3; i < groups.size(); i++) {
			ArrayList<Group> firstNGroups = (ArrayList<Group>) this.getFirstNFromList(groups, i);
			HashSet<Group> firstNGroupsAsSet = new HashSet<Group>();
			firstNGroupsAsSet.addAll(firstNGroups);
			ArrayList<HashSet<Group>> groupsCombinatoric = this.competitionGeneratorService.generateCombinatoric(firstNGroupsAsSet, this.groupsPerCompetition);
			ArrayList<Competition> competitions = this.transformGroupsCombinatoricIntoCompetition(groupsCombinatoric);
			
			competitionCounter = 1;
			for (Competition competition : competitions) {
				competitionCounter++;
				if (this.competitionHasAllDiferentTeams(competition)) {
					System.out.println(String.format("Combinatorio (%d, %d) - Competición nº %d", i, this.groupsPerCompetition, competitionCounter));
					return competition;
				}
			}
		}
		return null;
	}
	
	private boolean competitionHasAllDiferentTeams(Competition competition) {
		HashSet<Team> teams = new HashSet<Team>();
		int teamCounter = 0;
		for (Group group : competition.getGroups()) {
			teams.addAll(group.getTeams());
			teamCounter += group.getTeams().size();
		}
		if (teams.size() == teamCounter) {
			return true;
		}
		return false;
	}

	protected ArrayList<Competition> transformGroupsCombinatoricIntoCompetition(
			ArrayList<HashSet<Group>> groupsCombinatoric)
	{
		ArrayList<Competition> competitionsList = new ArrayList<Competition>();
		for (HashSet<Group> groups : groupsCombinatoric) {
			Competition competition = new Competition(this.competitionName, groups);
			competitionsList.add(competition);
		}
		return competitionsList;
	}

	protected ArrayList<Group> transformTeamsCombinatoricIntoGroup(ArrayList<HashSet<Team>> teamsCombinatoric) {
		ArrayList<Group> groups = new ArrayList<Group>(teamsCombinatoric.size());
		for (HashSet<Team> teamsSubset : teamsCombinatoric) {
			Group group = new Group(teamsSubset);
			groups.add(group);
		}
		return groups;
	}
	
	protected <T> List<T> getFirstNFromList(List<T> list, int n)
	{
		List<T> items = new ArrayList<T>();
		for (int i = 0; i < n; i++)
			items.add(list.get(i));
		return items;
	}
}
