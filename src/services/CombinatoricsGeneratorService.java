package services;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import main.App;

public class CombinatoricsGeneratorService<T> extends Service {

	public CombinatoricsGeneratorService(App app) {
		super(app);
	}
	
	/**
	 * Creates a combinatoric n,k with the objects of members's set
	 * where n = members.size() and k = membersPerGroup
	 * @param members
	 * @param membersPerGroup
	 * @return
	 */
	public ArrayList<HashSet<T>> generateCombinatoric(Set<T> members, int membersPerGroup) {
		ArrayList<HashSet<T>> groupsToReturn = new ArrayList<HashSet<T>>();
		
		if (members == null) {
			throw new IllegalArgumentException("Members cannot be null.");
		}
		
		if (membersPerGroup > members.size()) {
			throw new IllegalArgumentException("Members per group cannot be greater than members size.");
		}
		
		if (members.size() == membersPerGroup) {
			HashSet<T> group = new HashSet<T>(members.size());
			group.addAll(members);
			groupsToReturn.add(group);
			return groupsToReturn;
		}
		
		// Generate an array list to be able to get member by index
		ArrayList<T> membersAsList = new ArrayList<T>(members.size());
		membersAsList.addAll(members);
		int membersSize = members.size();
		
		int[] indices = new int[membersPerGroup]; // Keep indices pointing to elements in membersAsList
		if (membersPerGroup <= membersSize) {
			// first index sequence: 0, 1, 2, ...
			for (int i = 0; (indices[i] = i) < membersPerGroup - 1; i++);  
			groupsToReturn.add(getSubset(membersAsList, indices));
			while(true) {
				int i;
				// find position of item that can be incremented
				for (i = membersPerGroup - 1; i >= 0 && indices[i] == membersSize - membersPerGroup + i; i--); 
				if (i < 0) {
					break;
				}
				indices[i]++;
				// fill up remaining items
				for (++i; i < membersPerGroup; i++) {   
					indices[i] = indices[i - 1] + 1;
				}
				groupsToReturn.add(getSubset(membersAsList, indices));
			}
		}
		return groupsToReturn;
	}
	
	protected HashSet<T> getSubset(List<T> members, int[] subset) {
		HashSet<T> result = new HashSet<T>(subset.length); 
	    for (int i = 0; i < subset.length; i++) {
	    	result.add(members.get(subset[i]));
	    }
	    return result;
	}
}
