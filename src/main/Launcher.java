package main;

import java.awt.EventQueue;

import displays.swing.Swing;
import interfaces.DisplayInterface;

public class Launcher {
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DisplayInterface display = new Swing();
					App app = new App(display);
					app.init();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
