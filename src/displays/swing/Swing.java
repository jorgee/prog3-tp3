package displays.swing;

import java.awt.CardLayout;
import java.awt.Component;
import java.util.HashMap;
import javax.swing.JFrame;
import javax.swing.JPanel;

import controllers.Controller;
import displays.swing.views.*;
import interfaces.DisplayInterface;
import javax.swing.JMenuBar;
import javax.swing.JMenu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;

public class Swing implements DisplayInterface {

	private JFrame frame;
	private HashMap<String, SwingViewInterface> views = new HashMap<>();
	private Controller currentController;

	/**
	 * Create the application.
	 */
	public Swing() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		this.frame = new JFrame();
		this.frame.setSize(800, 800);
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.getContentPane().setLayout(new CardLayout(0, 0));
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu mnAsd = new JMenu("Archivo");
		menuBar.add(mnAsd);
		
		JMenu mnDisplay = new JMenu("Display");
		menuBar.add(mnDisplay);
		
		JMenuItem mntmConsola = new JMenuItem("Consola");
		mnDisplay.add(mntmConsola);
		
		frame.setVisible(true);
	}
	
	protected void deleteView(String viewName) {
		if (this.views.containsKey(viewName)) {
			this.views.remove(viewName);
		}
	}
	
	private SwingViewInterface getView(String viewName) throws Exception {
		if (this.views.containsKey(viewName)) {
			return this.views.get(viewName);
		} else {
			SwingViewInterface view = null;
			switch (viewName) {
				case "Main":
					view = new Main(this);
					break;
				default:
					throw new Exception("Undefined view " + viewName);
			}
			this.views.put(viewName, view);
			return view;
		}
	}
	
	public JFrame getContainer() {
		return this.frame;
	}

	@Override
	public void render(String viewName, boolean recreate) throws Exception {
		if (recreate) {
			this.deleteView(viewName);
		}
		SwingViewInterface view = this.getView(viewName);
		view.render();
		JPanel panel = (JPanel) view.getPanel();
		
		JPanel cards = (JPanel) this.frame.getContentPane();
		CardLayout cl = (CardLayout) cards.getLayout();
		cl.show(cards, viewName);
		
		if (this.getVisibleCard() != panel) {
			cards.add(panel, viewName);
			cl.show(cards, viewName);
		}
	}
	
	@Override
	public void render(String viewName) throws Exception {
		this.render(viewName, false);
	}

	public void setController(Controller controller) {
		this.currentController = controller;
	}

	@Override
	public Controller getController() {
		return this.currentController;
	}
	
    private Component getVisibleCard() {
        for(Component c: this.frame.getComponents()) {
            if (c.isVisible())
                return c;
        }
        return null;
    }
    
    protected void addNewGameEvent(Component component, int rows, int columns) {
    	((JMenuItem) component).addActionListener(
			new ActionListener() {
				public void actionPerformed(ActionEvent ev) {
	    			try {
						//getController().actionStartNewGame(rows, columns);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			}
    	);
    }
}
