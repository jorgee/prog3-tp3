package displays.swing.events;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import controllers.Controller;
import controllers.MainController;
import displays.swing.Swing;
import displays.swing.views.Main;
import entities.Competition;
import entities.Team;

public class GenerateBalancedCompetitionButtonClicked implements ActionListener {

	Swing swingDisplay;
	Main view;
	
	public GenerateBalancedCompetitionButtonClicked(Main view, Swing swingDisplay) {
		this.swingDisplay = swingDisplay;
		this.view = view;
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {
        this.view.getButtonsInView().get("btnCompetenciaBalanceada").setEnabled(false);

		Controller controller = this.swingDisplay.getController();
        if (controller instanceof MainController) {
        	MainController mainController = (MainController) controller; 
        	for (Team team : view.getTeams()) {
        		mainController.actionRegisterTeam(team);;
        	}
        	Competition competition = mainController.getBalancedGroupsCompetition();

    		this.view.generateCompetitionGroupsView(competition, this.view.getPanelsInView().get("mainPanel.centralPanel"), 50, 515, 240);
    		this.view.getPanel().repaint();
        }

        this.view.getButtonsInView().get("btnCompetenciaBalanceada").setEnabled(true);
	}
}
