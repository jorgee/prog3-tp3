package displays.swing.views;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JPanel;

import displays.swing.Swing;
import displays.swing.SwingViewInterface;
import displays.swing.events.GenerateBalancedCompetitionButtonClicked;
import entities.Competition;
import entities.Group;
import entities.Team;
import main.Launcher;

import javax.swing.border.LineBorder;

import java.awt.Color;

import javax.swing.JButton;
import java.awt.Dimension;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import java.awt.SystemColor;

public class Main implements SwingViewInterface {
	
	private JPanel panel;
	private Swing swingDisplay;
	private HashMap<String, JButton> buttonsInView;
	private HashMap<String, JPanel> panelsInView;
	private ArrayList<Team> teams;
	
	public Main(Swing swingDisplay) {
		this.swingDisplay = swingDisplay;
		this.buttonsInView = new HashMap<String, JButton>();
		this.panelsInView = new HashMap<String, JPanel>();
		this.teams = new ArrayList<Team>(12);
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	@Override
	public void render() {
        JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout(0, 0));
        panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		this.panel = panel;
		this.panelsInView.put("mainPanel", panel);

		this.swingDisplay.getContainer().getContentPane().add(this.getPanel(), "mainViewPanel");
		
		JPanel panelSur = new JPanel();
		panelSur.setPreferredSize(new Dimension(10, 80));
		panelSur.setBackground(Color.ORANGE);
		panel.add(panelSur, BorderLayout.SOUTH);
		panelSur.setLayout(null);
		this.panelsInView.put("mainPanel.southPanel", panelSur);
		
		JButton btnGenerarEnlaces = new JButton("Generar grupos balanceados");
		btnGenerarEnlaces.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnGenerarEnlaces.setBounds(34, 28, 235, 25);
		btnGenerarEnlaces.addActionListener(new GenerateBalancedCompetitionButtonClicked(this, this.swingDisplay));
		this.buttonsInView.put("btnCompetenciaBalanceada", btnGenerarEnlaces);
		
		panelSur.add(btnGenerarEnlaces);
		
		JPanel panelCentral = new JPanel();
		panel.add(panelCentral, BorderLayout.CENTER);
		panelCentral.setLayout(null);
		this.panelsInView.put("mainPanel.centralPanel", panelCentral);

		Team argentina = new Team("Argentina", 85);
		this.teams.add(argentina);
		Team brasil = new Team("Brasil", 88);
		this.teams.add(brasil);
		Team bolivia = new Team("Bolivia", 65);
		this.teams.add(bolivia);
		Team colombia = new Team("Colombia", 78);
		this.teams.add(colombia);
		Team catar = new Team("Catar", 60);
		this.teams.add(catar);
		Team japon = new Team("Japon", 69);
		this.teams.add(japon);
		Team venezuela = new Team("Venezuela", 67);
		this.teams.add(venezuela);
		Team peru = new Team("Peru", 74);
		this.teams.add(peru);
		Team uruguay = new Team("Uruguay", 77);
		this.teams.add(uruguay);
		Team paraguay = new Team("Paraguay", 71);
		this.teams.add(paraguay);
		Team chile = new Team("Chile", 77);
		this.teams.add(chile);
		Team ecuador = new Team("Ecuador", 67);
		this.teams.add(ecuador);
		
		this.generateTeamListDisplay(this.teams, panelCentral);
		
		JLabel lblLogoCopaLabel = new JLabel("");
		lblLogoCopaLabel.setIcon(new ImageIcon(Launcher.class.getResource("/copa_america_2019_500x500.png")));
		lblLogoCopaLabel.setBounds(125, 10, 500, 500);
		panelCentral.add(lblLogoCopaLabel);
		
		JLabel lblCoeficienteDeFuerza = new JLabel("Coef. de Fuerza");
		lblCoeficienteDeFuerza.setForeground(SystemColor.inactiveCaption);
		lblCoeficienteDeFuerza.setBounds(150, 12, 114, 15);
		panelCentral.add(lblCoeficienteDeFuerza);
		
		lblCoeficienteDeFuerza = new JLabel("Coef. de Fuerza");
		lblCoeficienteDeFuerza.setForeground(Color.GRAY);
		lblCoeficienteDeFuerza.setBounds(415, 12, 114, 15);
		panelCentral.add(lblCoeficienteDeFuerza);
		
		lblCoeficienteDeFuerza = new JLabel("Coef. de Fuerza");
		lblCoeficienteDeFuerza.setForeground(Color.GRAY);
		lblCoeficienteDeFuerza.setBounds(685, 12, 114, 15);
		panelCentral.add(lblCoeficienteDeFuerza);
	}

	@Override
	public Component getPanel() {
		return this.panel;
	}

	public HashMap<String, JButton> getButtonsInView() {
		return this.buttonsInView;
	}
	
	public HashMap<String, JPanel> getPanelsInView() {
		return this.panelsInView;
	}
	
	protected void generateTeamListDisplay(ArrayList<Team> teams, JPanel panel) {
		int rows = 4;
		int rowCounter = 1;
		int columnCounter = 1;
		int rowSpacing = 50;
		int columnSpacing = 250;
		int initialX = 20;
		int initialY = 20;
		for (Team team : teams) {
			JPanel panelCentral = new JPanel();
			panel.add(panelCentral, BorderLayout.CENTER);
			panelCentral.setLayout(null);
			
			JLabel lblTeam = new JLabel(team.getName());
			lblTeam.setIcon(new ImageIcon(Launcher.class.getResource("/"+team.getName()+"_48x48.png")));
			int x = initialX * columnCounter + ((columnCounter - 1) * columnSpacing);
			int y = initialY * rowCounter + ((rowCounter - 1) * rowSpacing);
			lblTeam.setBounds(x, y, 160, 48);
			panel.add(lblTeam);
			
			JTextField textFieldTeamCoef = new JTextField();
			textFieldTeamCoef.setEnabled(false);
			textFieldTeamCoef.setText(Float.toString(team.getCoeficienteDeFuerza()));
			x = initialX * columnCounter + 135 + ((columnCounter - 1) * columnSpacing);
			y = initialY * rowCounter + 15 + ((rowCounter - 1) * rowSpacing);
			textFieldTeamCoef.setBounds(x, y, 90, 20);
			panel.add(textFieldTeamCoef);
			
			rowCounter += 1;
			if (rowCounter > rows) {
				columnCounter += 1;
				rowCounter = 1;
			}
		}
	}
	
	/**
	 * 
	 * @param group
	 * @param groupTitle
	 * @param dispersion
	 * @param posx
	 * @param posy
	 * @param counter Es usado para cambiar el color
	 * @return
	 */
	protected JPanel generateGroupPanel(Group group, String groupTitle, float dispersion, int posx, int posy, int counter) {
		JPanel groupPanel = new JPanel();
		Color color = null;
		int modulo = counter % 3;
		switch (modulo) {
			case 0:
				color = new Color(252, 204, 00); // Amarillo
				break;
			case 1:
				color = new Color(85, 203, 215); // Celeste
				break;
			case 2:
				color = new Color(47, 156, 47); // Verde
				break;
		}
			
		new Color(143, 188, 143);
		groupPanel.setBackground(color);
		groupPanel.setBounds(posx, posy, 200, 150);
		groupPanel.setLayout(null);
		
		JLabel lblGroupTitle = new JLabel(groupTitle);
		lblGroupTitle.setForeground(Color.WHITE);
		lblGroupTitle.setBounds(70, 5, 66, 15);
		groupPanel.add(lblGroupTitle);
		
		int rows = 2;
		int rowCounter = 1;
		int columnCounter = 1;
		int rowSpacing = 40;
		int columnSpacing = 75;
		int initialX = 25;
		int initialY = 25;
		for (Team team : group.getTeams()) {
			
			JLabel lblTeam = new JLabel();
			JLabel lblTeamName = new JLabel(team.getName().substring(0, 3).toUpperCase());
			lblTeam.setIcon(new ImageIcon(Launcher.class.getResource("/"+team.getName()+"_48x48.png")));
			int x = initialX * columnCounter + ((columnCounter - 1) * columnSpacing);
			int y = initialY * rowCounter + ((rowCounter - 1) * rowSpacing);
			lblTeam.setBounds(x, y, 75, 48);
			lblTeamName.setForeground(Color.WHITE);
			lblTeamName.setBounds(x + 9, y + 30, 75, 48);
			groupPanel.add(lblTeamName);
			groupPanel.add(lblTeam);
			
			rowCounter += 1;
			if (rowCounter > rows) {
				columnCounter += 1;
				rowCounter = 1;
			}
		}
		return groupPanel;
	}
	
	public void generateCompetitionGroupsView(
			Competition competition, 
			JPanel panelToAdd, 
			int initialPosX, 
			int initialPosY,
			int spacing
	) {
		int groupChar = 65; // A
		int counter = 0;
		
		for (Group group : competition.getGroups()) {
			int posX = initialPosX + counter * spacing;
			int posY = initialPosY;
			counter++;
			panelToAdd.add(
					this.generateGroupPanel(
							group, 
							"Grupo " + Character.toString ((char) groupChar), 
							group.getDispersion(), 
							posX, 
							posY,
							counter
					)
			);
			groupChar++;
		}
	}
	
	public ArrayList<Team> getTeams() {
		return this.teams;
	}
}
