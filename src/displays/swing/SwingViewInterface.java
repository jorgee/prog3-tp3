package displays.swing;

import java.awt.Component;

public interface SwingViewInterface {

	public void render();

	public Component getPanel();
}
